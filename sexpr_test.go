/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package sexpr_test

import (
	"os"
	"testing"

	"idio.link/go/sexpr"
)

func TestParsesFile(t *testing.T) {
	fname := "test.sxp"
	f, err := os.Open(fname)
	if err != nil {
		t.Errorf("failed to open file: %s\n", fname)
	}
	ast := sexpr.NewAst()
	if err = ast.ParseFile(f); err != nil {
		t.Errorf("failed to parse file %s: %s\n", fname, err)
	}
}
