/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package sexpr

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

const debug = false

type AstBranch struct {
}

func (a *AstBranch) Value() *bytes.Buffer {
	return new(bytes.Buffer)
}

func (a *AstBranch) event(sm *stackMachine, e byte) error {
	switch {
	case e == '\n':
		sm.Substate = smSubstateDefault
		sm.LineCount += 1
		sm.CharCount = 0
	case sm.Substate == smSubstateComment:
	case e == '#':
		sm.Substate = smSubstateComment
	case e == '\r':
		sm.CharCount -= 1
	case e == '[':
		node := AstNode{Token: &AstBranch{}}
		sm.Stack = append(sm.Stack, &node)
	case e == ']':
		err := sm.foldOnce()
		if err != nil &&
			err.Error() == "fold: insufficient material" {
			return sm.issueError("encountered excess ']'")
		}
	case e == ' ' || e == '\t':
	case 'A' <= e && e <= 'Z' ||
		'a' <= e && e <= 'z':
		node := &AstNode{Token: &AstAtom{}}
		sm.Stack = append(sm.Stack, node)
		sm.accrete(e)
	case '0' <= e && e <= '9':
		node := &AstNode{Token: &AstNat{}}
		sm.Stack = append(sm.Stack, node)
		sm.accrete(e)
	case e == '\'':
		node := &AstNode{Token: &AstString{}}
		sm.Stack = append(sm.Stack, node)
	default:
		return sm.issueError(fmt.Sprintf("unexpected input %q", e))
	}
	return nil
}

type AstComment struct {
}

func (a *AstComment) Value() *bytes.Buffer {
	var buf bytes.Buffer
	return &buf
}

func (a *AstComment) event(sm *stackMachine, e byte) error {
	return nil
}

type AstAtom struct {
	value *bytes.Buffer
}

func (a *AstAtom) Value() *bytes.Buffer {
	if a.value == nil {
		var buf bytes.Buffer
		a.value = &buf
	}
	return a.value
}

func (a *AstAtom) event(sm *stackMachine, e byte) error {
	switch {
	case e == ']':
		sm.foldOnce()
		err := sm.foldOnce()
		if err != nil &&
			err.Error() == "fold: insufficient material" {
			return sm.issueError("encountered excess ']'")
		}
	case e == ' ' || e == '\t':
		sm.foldOnce()
	case e == '\r':
		sm.foldOnce()
	case e == '\n':
		sm.LineCount += 1
		sm.CharCount = 0
		sm.foldOnce()
	case '0' <= e && e <= '9' ||
		'A' <= e && e <= 'Z' ||
		'a' <= e && e <= 'z' ||
		e == '-' ||
		e == '.' ||
		e == '/' ||
		e == '?' ||
		e == '_':
		sm.accrete(e)
	default:
		return sm.issueError(fmt.Sprintf("unexpected input %q", e))
	}
	return nil
}

type AstNat struct {
	value *bytes.Buffer
}

func (a *AstNat) Value() *bytes.Buffer {
	if a.value == nil {
		var buf bytes.Buffer
		a.value = &buf
	}
	return a.value
}

func (a *AstNat) event(sm *stackMachine, e byte) error {
	switch {
	case e == ']':
		sm.foldOnce()
		err := sm.foldOnce()
		if err != nil &&
			err.Error() == "fold: insufficient material" {
			return sm.issueError("encountered excess ']'")
		}
	case e == ' ' || e == '\t':
		sm.foldOnce()
	case e == '\r':
		sm.foldOnce()
	case e == '\n':
		sm.LineCount += 1
		sm.CharCount = 0
		sm.foldOnce()
	case '0' <= e && e <= '9':
		sm.accrete(e)
	default:
		return sm.issueError(fmt.Sprintf("unexpected input %q", e))
	}
	return nil
}

type AstString struct {
	value *bytes.Buffer
}

func (a *AstString) Value() *bytes.Buffer {
	if a.value == nil {
		a.value = new(bytes.Buffer)
	}
	return a.value
}

func (a *AstString) event(sm *stackMachine, e byte) error {
	switch {
	case sm.Substate == smSubstateEscape:
		var s byte
		switch e {
		case 'n':
			s = '\n'
		case 't':
			s = '\t'
		}
		sm.accrete(s)
		sm.Substate = smSubstateDefault
	case e == '\\':
		sm.Substate = smSubstateEscape
	case e == '\'':
		sm.foldOnce()
	case e == '\r':
		sm.CharCount -= 1
	case e == '\n':
		sm.LineCount += 1
		sm.CharCount = 0
	default:
		sm.accrete(e)
	}
	return nil
}

func AstTokenTypeToString(t AstToken) string {
	switch t.(type) {
	case *AstBranch:
		return "sexpr"
	case *AstAtom:
		return "atom"
	case *AstComment:
		return "comment"
	case *AstNat:
		return "nat"
	case *AstString:
		return "str"
	default:
		panic("unknown token type")
	}
}

type sexprCursor struct {
	Node  *AstNode
	Index int
}

type AstToken interface {
	event(*stackMachine, byte) error
	Value() *bytes.Buffer
}

func NewAst() *AstNode {
	return &AstNode{Token: &AstBranch{}}
}

type AstNode struct {
	Nodes []*AstNode
	Token AstToken
}

func (s *AstNode) ParseFile(
	f *os.File,
) error {
	r := bufio.NewReader(f)
	st := make([]*AstNode, 0, 16)
	sm :=
		&stackMachine{
			Stack:     st,
			LineCount: 1,
		}
	sm.Stack = append(sm.Stack, s)
	for {
		b, err := r.ReadByte()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("read file: %s\n", err)
		}
		if err = sm.Event(b); err != nil {
			return err
		}
		if debug {
			fmt.Fprintf(os.Stderr, "debug: %s\n", sm.formatStatus())
		}
	}
	if debug {
		sm.Stack[0].Print()
	}
	return nil
}

func (s *AstNode) Print() {
	stack := make([]*sexprCursor, 0, 15)
	cur := &sexprCursor{Node: s}
	stack = append(stack, cur)
	var next *sexprCursor
	var last int
	for {
		if len(stack) == 0 {
			return
		}
		last = len(stack) - 1
		cur = stack[last]
		token := cur.Node.Token
		if cur.Index >= len(cur.Node.Nodes) {
			switch token.(type) {
			case *AstBranch:
				if len(cur.Node.Nodes) == 0 {
					fmt.Print("[]")
				} else {
					fmt.Print("]")
				}
			case *AstAtom:
				fmt.Printf("%s", token.Value().Bytes())
			case *AstString:
				str := fmt.Sprintf("%q", token.Value().Bytes())
				fmt.Printf("%s", strings.ReplaceAll(str, "\"", "'"))
			case *AstNat:
				fmt.Printf("%s", token.Value().Bytes())
			}
			stack[last] = nil
			stack = stack[:last]
			continue
		} else if cur.Index == 0 {
			switch token.(type) {
			case *AstBranch:
				fmt.Print("[")
			default:
			}
		} else {
			switch token.(type) {
			case *AstBranch:
				fmt.Print(" ")
			default:
			}
		}
		next = &sexprCursor{Node: cur.Node.Nodes[cur.Index]}
		stack = append(stack, next)
		cur.Index += 1
	}
}

type ParseError struct {
	Msg  string
	Buf  []byte
	Line int
	Char int
}

func (e ParseError) Error() string {
	return fmt.Sprintf("%s at line %d, character %d", e.Msg, e.Line, e.Char)
}

type smSubstate int

const (
	smSubstateDefault smSubstate = iota
	smSubstateEscape
	smSubstateComment
)

type stackMachine struct {
	Stack     []*AstNode
	LineCount int
	CharCount int
	Substate  smSubstate
}

func (sm *stackMachine) formatStatus() string {
	err := sm.issueError("").(ParseError)
	return fmt.Sprintf(
		"line: %d, char: %d, stacklen: %d, state: %s, token: %s",
		err.Line,
		err.Char,
		len(sm.Stack),
		AstTokenTypeToString(sm.top().Token),
		err.Buf,
	)
}

func (sm *stackMachine) accrete(b byte) error {
	return sm.top().Token.Value().WriteByte(b)
}

func (sm *stackMachine) foldOnce() error {
	last := len(sm.Stack) - 1
	if last <= 0 {
		return errors.New("fold: insufficient material")
	}
	sm.Stack[last-1].Nodes =
		append(sm.Stack[last-1].Nodes, sm.Stack[last])

	sm.Stack[last] = nil
	sm.Stack = sm.Stack[:last]

	return nil
}

func (sm *stackMachine) issueError(msg string) error {
	return ParseError{
		Msg:  msg,
		Buf:  sm.top().Token.Value().Bytes(),
		Line: sm.LineCount,
		Char: sm.CharCount,
	}
}

func (sm *stackMachine) top() *AstNode {
	if len(sm.Stack) == 0 {
		return NewAst()
	}
	return sm.Stack[len(sm.Stack)-1]
}

func (sm *stackMachine) Event(e byte) error {
	sm.CharCount += 1
	return sm.top().Token.event(sm, e)
}
